# Retroshare Web Bridges

Retroshare web based bridges to view content on Retroshare network through responsive html interface.


* Desktop:

![Desktop devices](imgs/desktop.png)

* Phone:

![Little devices](imgs/phone.png)

## How to:

### Install Retroshare and prepare it to get content

Use this guide for the purpose: https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md

### Install retroshare-web-bridges

1. Clone, link or copy the necessary html files to your web server directory. If you just want `channels.html` working copy just the scripts `scripts/utils.js` and `scripts/channelUi.js`. Also `css/w3.css` file is needed.
2. Merge the necessary `nginx.conf` files to _reverse proxy_ the Retroshare relay. Each conf file  has the necessary configurations to expose the proper api parts and hardcoded data. For example, if you want that your nginx supports auto-join and channels you should manually open each file on `nginx.conf/`, hardcode necessary data and and copy/paste it to your server nginx configuration file. Look at `documentation/` of each project for further information.

### Web bridges libs

 Web bridges uses the following third party libraries:

 * [w3.css](https://www.w3schools.com/w3css/) a free to use css library.
