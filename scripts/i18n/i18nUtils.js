var loadLocales = function(langs){

  i18next
    .use(i18nextBrowserLanguageDetector)
    .init({
      fallbackLng: 'en',
      //debug: true,
      resources : langs,
    }, function(err, t) {
      // init set content
      updateContent();
    });
}
function updateContent() {
  var el = document.getElementsByClassName("translate")
  for (var i = 0; i < el.length; i++) {
    var args = el[i].getAttribute('data-args');
    if (args) {
      translation = i18next.t(args)
      if (translation)   el[i].innerHTML = translation;
      else el[i].innerHTML = args
    }
  }
}
function changeLng(lng) {
  i18next.changeLanguage(lng);
  updateContent();
}
