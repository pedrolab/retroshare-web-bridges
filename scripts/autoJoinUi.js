// Copy to clipboard function
function copyToClipboard(element) {
  if (typeof element === "string") {
    var element = document.getElementById(element)
  }
  element.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  console.log("Copied the text: " + element.value);
  showSnackBar("Copied to clipboard!")
}


// Check on index.html to put the node name on diferent parts
function setNodeInfo (info)
{
  var info = info || nodeInfo;

  function setTextOnSpan(objs, text)
  {
    for (var i = 0; i < objs.length; i++) {
      objs[i].innerHTML = text
    }
  }

  var locationName = document.getElementsByClassName("mLocationName");
  setTextOnSpan (locationName, info.mLocationName)

  var pgpName = document.getElementsByClassName("mPpgName");
  setTextOnSpan (pgpName, info.mPpgName)

  document.title = info.mLocationName + " on " + info.mPpgName + " bootstrap node"
}

// Try to get/parse/and check node info from info file.
// Else uses the defaults node info
function getNodeInfo()
{
  // Try to get jsonninfo file
  rsJsonApiRequest (infoFile , "" , function (p)
  {
    var nodeData;
    try
    {
      var data = JSON.parse(p);
      console.log("Get node info", data);
      // Check if has needed properties
      if(data.hasOwnProperty('mPpgName') && data.hasOwnProperty('mLocationName'))
      {
        nodeInfo = data;
      }
    }
    catch (e)
    {
      console.log(infoFile , " not found or bad parsing error");
    }
    setNodeInfo();
  }, null,"GET");
}


function showSnackBar(text, t)
{
  var t = t || 3000

  // Get the snackbar DIV
  var x = document.getElementById("snackbar");

  var close = '<span onclick="this.parentElement.style.display=\'none\'" class="w3-button w3-display-topright">X</span>'

  x.innerHTML = close+text;

  // Add the "show" class to DIV
  x.className = "show";

  // After t seconds, remove the show class from DIV
  setTimeout(function(){
    // Do not hide the element
    if (t < 0)
    {
      return 0;
    }
    x.className = x.className.replace("show", "hide");

    setTimeout(function(){
      x.className = x.className.replace("hide", "");

    }, 500)
  }, t);
}

// Function that interacts with the ui to disable everything when node is down
function handleNodeDown (xhr)
{
  console.log("nodedown");
  nodeCertificate.disabled = true;
  guestCertificate.disabled = true;
  var btns = document.getElementsByClassName("rsBtn")
  for (var i = 0; i < btns.length; i++)
  {
    btns[i].disabled = true;
  }
  showSnackBar('<span class="unicodeWarnings">&#x26A0;&#x26A0;</span><br>It seems that relay node is down<br>Please try again later or contact the site admins', -1)
}
