// This script contain the logic of the channel ui
var sidebarId="chanSidebar"
var activeChannel= "";
// Used to set active button
var activeButton= "";
// Set this to enable download directory
var downloadDir = "/file/"


// Show content of channel based on id
function fillContentPage (id)
{
  if (activeChannel == id)
  {
    return 0;
  }
  else if (activeButton === "")
  {
    activeButton = document.getElementById("btn"+id)
    activateBtn (activeButton)
  }
  activeChannel = id;

  // Set header
  var chanInfo = getChannel (id)
  document.getElementById("chanTitle").innerHTML = chanInfo.mMeta.mGroupName
  document.getElementById("chanDescription").innerHTML = chanInfo.mDescription

  // Set posts
  var postList = document.getElementById("postList")
  postList.innerHTML = " "
  getChannelContent(id).forEach(function(p) {
    console.log(p);
    postList.appendChild(getPostCard(p))
  });
}

// Return json channel object based on id
function getChannel (id)
{
  var i = channels.channelsInfo.findIndex(function(c, i)
  {
    return c.mMeta.mGroupId === id
  })
  return channels.channelsInfo[i]
}

// Get all posts from one channel
// Return array of posts
function getChannelContent (id)
{
  var posts = new Array()
  var i = channelsContents.posts.findIndex(function(c, i)
  {
    if (c.mMeta.mGroupId === id)
    {
      posts.push(c)
    }
  })
  return posts
}

// Generate channel menu card (for sidenav) from json info get from the api.
function getChanMenuCard (chan)
{
  var template =  '<div href="#" id="btn'+chan.mMeta.mGroupId+'" class=" w3-bar-item w3-button w3-container w3-border">' +
                      '<img class="w3-left w3-circle w3-margin-right" style="width:85px">' +
                      '<p class="">' +
                        '<span class="w3-large">'+
                          chan.mMeta.mGroupName+
                        ' </span>'+
                        '<span><a href="'+getRSLinkChannelForum("channel", chan.mMeta.mMsgName, chan.mMeta.mGroupId)+'"target="_blank" style="font-size:25px;width:100%;text-decoration: none;">&#x1F517;</a></span>'+
                      '</p>'+
                  '</div>';
                   //
  var card = createElementFromHTML(template);
  card.addEventListener("click", function() {
    fillContentPage(chan.mMeta.mGroupId)
    desactivateBtn (activeButton)
    activeButton = this;
    activateBtn(activeButton)
  }, false);

  // Add info to the card
  // card.getElementsByTagName("SPAN")[0].innerHTML = ;
  card.getElementsByTagName("IMG")[0].setAttribute('src' , 'data:image/png;base64,'+chan.mImage.mData);

  return card
}

// Get card of a post
function getPostCard (post)
{
  var template =  '<li class="w3-bar w3-animate-left">'+
                    '<div class="w3-row">'+
                    '<div class="w3-container  w3-col w3-center w3-mobile"  style="width:150px">'+
                      '<div  class="w3-teal" style="" >'+
                        '<a href ="'+sanitizeUrl(downloadDir+post.mFiles[0].mName)+'" class="w3-button " target="_blank" style="font-size:25px;width:100%;">&#x21E9;</a>'+
                        '<hr style="margin:0px;">' +
                        '<a href ="'+getRSLinkFile(post.mFiles[0] )+'" class="w3-button " target="_blank" style="font-size:25px;width:100%;">&#x1F517;</a>'+
                      '</div>'+
                    '</div>'+
                      '<div class=" w3-container w3-col"  style="width:90%">'+
                        '<img class="w3-bar-item" style="width:150px">'+
                        '<div class="w3-bar-item">'+
                          '<a class="chanlink w3-large" style="font-weight:bold;" href="'+getRSLinkChannelForum('channel', post.mMeta.mMsgName, post.mMeta.mGroupId, post.mMeta.mMsgId)+'"></a><br>'+
                          '<span></span>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</li>';
  var card = createElementFromHTML(template);

  card.getElementsByClassName("chanlink")[0].innerHTML = post.mMeta.mMsgName;
  card.getElementsByTagName("SPAN")[0].innerHTML = getDateFromEpoch(post.mMeta.mPublishTs);
  card.getElementsByTagName("IMG")[0].setAttribute('src' , 'data:image/png;base64,'+post.mThumbnail.mData);
  return card
}

// Add an object to the sidebar
function addToSidebar (obj)
{
  document.getElementById(sidebarId).appendChild(obj);
}


// Open/close channel menu
function chanMenuOpen()
{
    document.getElementById(sidebarId).style.display = "block";
    document.getElementById("chanOverlay").style.display = "block";
}
function chanMenuClose()
{
    document.getElementById(sidebarId).style.display = "none";
    document.getElementById("chanOverlay").style.display = "none";
}

function activateBtn (btn)
{
  btn.classList.add("w3-text-teal");
}

function desactivateBtn (btn)
{
  btn.classList.remove("w3-text-teal");
}
