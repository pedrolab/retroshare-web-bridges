# View forums on HTML

### Subscribe

1. Get all available channels:

```
curl -u "$ACCOUNT:$PASSWORD"  http://127.0.0.1:9092/RsGxsChannels/getChannelsSummaries
```

2. Select a channel and subscribe to it.  **TODO**: for some reason this doesn't work, finally subscribed via GUI.

```
curl -u "$ACCOUNT:$PASSWORD"  -d '{ "groupId":"e78125439fff723b3b15bb77b8f25dba" , "subscribe" : "true" }' http://127.0.0.1:9092/RsGxsChannels/subscribeToGroup
```


### Configure to protect

Get `chanIds` that you want to serve and hardcode it on `nginx.conf`. This is used to not leak other information.

So you can only request the message of the specific forums that you configure.

For more info see `nginx.conf/channels.nginx.conf`.

### Calls

* Get chanel info (as avatar, description, name...)

```
curl -u "$ACCOUNT:$PASSWORD"  -d '{ "chanIds": [ "e78125439fff723b3b15bb77b8f25dba" ] }' http://127.0.0.1:9092/RsGxsChannels/getChannelsInfo
```

* Get channel content.
```
curl -u "$ACCOUNT:$PASSWORD"  -d '{ "chanIds": [ "e78125439fff723b3b15bb77b8f25dba" ] }' http://127.0.0.1:9092/RsGxsChannels/getChannelsContent
```
